public class MethodsTest {
    public static void main (String [] args) {

        int x = 10;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);
        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x+50);
        methodTwoInputNoReturn(13, 24.7);
        int z = methodNoInputReturnInt();
        System.out.println(z);
        double h = sumSquareRoot(6, 3);
        System.out.println(h);
        String s1 = "hello";
        String s2 = "goodbye";
        System.out.println(s1.length());
        System.out.println(s2.length());
        int o = SecondClass.addOne(50);
        System.out.println(o);
        SecondClass sc = new SecondClass();
        int p = sc.addTwo(50);
        System.out.println(p);

    }
    public static void methodNoInputNoReturn(){
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 50;
        System.out.println(x);
    }
    public static void methodOneInputNoReturn (int a){
        System.out.println("Inside the method one input no return");
        System.out.println(a);
    }
    public static void methodTwoInputNoReturn (int b, double c){
        System.out.println(b);
        System.out.println(c);
    }
    public static int methodNoInputReturnInt(){

        return 6;
    }
    public static double sumSquareRoot(int d, int e){
        int f = d + e;
        double g = Math.sqrt(f);
        return g;
    }
}
