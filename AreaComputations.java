public class AreaComputations {
    public static double areaSquare(double lengthOneSide){
        double areaOfSquare = lengthOneSide * lengthOneSide;
        return areaOfSquare;
    }
    public double areaRectangle(double length, double width){
        double areaOfRectangle = length * width;
        return areaOfRectangle;
    }
}
