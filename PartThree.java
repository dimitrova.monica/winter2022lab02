import java.util.Scanner;
public class PartThree {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        System.out.println("Enter the length of one side of a square: ");
        double lengthOneSide = reader.nextDouble();
        System.out.println("Enter the length of a rectangle: ");
        double length = reader.nextDouble();
        System.out.println("Enter the width of a rectangle: ");
        double width = reader.nextDouble();

        //calls a static method
        double squareArea = AreaComputations.areaSquare(lengthOneSide);
        System.out.println("The area of the square is: " + squareArea);

        //calls an instance method
        AreaComputations sc = new AreaComputations();
        double rectangleArea = sc.areaRectangle(length, width);
        System.out.println("The area of the rectangle is: " + rectangleArea);
    }
}
